import os

turn = ""
switcher = True

mapa = [ ["", "", ""], ["", "", ""], ["", "", ""] ]

def _assign(mapa, posx, posy, turn):
	if (mapa[posx][posy] == ""):
		mapa[posx][posy] = turn
	return mapa

def _assert(mapa):
	# Assert for 1st row equal except for empty strings, otherwise, the game will always start with "" as winner
	if ((mapa[0][0] == mapa[0][1] and mapa[0][1] == mapa[0][2]) and (mapa[0][0] != "")):
		return mapa[0][0]

	# Assert for 2nd row equal except for empty strings, otherwise, the game will always start with "" as winner
	elif ((mapa[1][0] == mapa[1][1] and mapa[1][1] == mapa[1][2]) and (mapa[1][0] != "")):
		return mapa[1][0]

	# Assert for 3rd row equal except for empty strings, otherwise, the game will always start with "" as winner
	elif ((mapa[2][0] == mapa[2][1] and mapa[2][1] == mapa[2][2]) and (mapa[2][0] != "")):
		return mapa[2][0]

	# Assert for Left to Right diagonal line except for empty strings, otherwise, the game will always start with "" as winner
	elif ((mapa[0][0] == mapa[1][1] and mapa[1][1] == mapa[2][2]) and (mapa[0][0] != "")):
		return mapa[0][0]

	# Assert for Right to Left diagonal line except for empty strings, otherwise, the game will always start with "" as winner
	elif ((mapa[0][2] == mapa[1][1] and mapa[1][1] == mapa[2][0]) and (mapa[0][2] != "")):
		return mapa[0][2]

	# Assert for 1st column equal except for empty strings, otherwise, the game will always start with "" as winner
	if ((mapa[0][0] == mapa[1][0] and mapa[1][0] == mapa[2][0]) and (mapa[0][0] != "")):
		return mapa[0][0]

	# Assert for 2nd column equal except for empty strings, otherwise, the game will always start with "" as winner
	elif ((mapa[0][1] == mapa[1][1] and mapa[1][1] == mapa[2][1]) and (mapa[0][1] != "")):
		return mapa[0][1]

	# Assert for 3rd column equal except for empty strings, otherwise, the game will always start with "" as winner
	elif ((mapa[0][2] == mapa[1][2] and mapa[1][2] == mapa[2][2]) and (mapa[0][2] != "")):
		return mapa[0][2]

	# Return empty string if no cases matched
	return ""

def printmap(mapa):
	os.system("clear");
	for row in mapa:
		print(row)

printmap(mapa)
pos = 0
while (True):
	try:
		turn = "X" if switcher else "O"

		pos = input("{0} turn. Enter position: ".format(turn))
		posx = int((pos.split(",")[0]).strip()) - 1
		posy = int((pos.split(",")[1]).strip()) - 1
		mapa = _assign(mapa, posx, posy, turn)
		res = _assert(mapa)
		printmap(mapa)
		if (res != ""):
			print("{0} wins".format(res))
			break
		switcher = not switcher
	except:
		break
